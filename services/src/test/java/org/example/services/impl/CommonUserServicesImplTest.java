package org.example.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.services.AdminService;
import org.example.services.CommonUserService;
import org.example.services.ServiceProvider;
import org.junit.Test;

public class CommonUserServicesImplTest {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    @Test
    public void test() throws Exception{

        final ServiceProvider serviceProvider = ServiceProvider.getInstance();
        final CommonUserService commonService = serviceProvider.getCommonUserService();
        final AdminService adminService = serviceProvider.getAdminService();

//        commonService.registration("ru", "ru", "ru", "ru", "teacher");
//        System.out.println();

//       LOGGER.info("create_____ " + adminService.createUser("ops", "kind", "Gorg", "Pologin", "3"));
//       LOGGER.info("create_____ " + adminService.createUser("lo", "lo", "Gorg", "Pologin", "2"));
//
//       LOGGER.info(commonService.registration("loop", "coop", "Kool", "Lol", "3"));

        LOGGER.info(commonService.authorize("ops", "kind") + "--------------authorize");
        System.out.println();
        LOGGER.info("teachers-----------" + commonService.getTeachers());
        System.out.println();
//        LOGGER.info(commonService.authorize("asd", "asd"));
        LOGGER.info(commonService.getSubjectInfo("Harmony").getSpecification());

        System.out.println();

    }
}
