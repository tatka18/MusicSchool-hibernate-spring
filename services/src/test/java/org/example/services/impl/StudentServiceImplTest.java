package org.example.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.services.ServiceProvider;
import org.example.services.StudentService;
import org.junit.Test;

public class StudentServiceImplTest {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);
    @Test
    public void test() throws Exception {

        final ServiceProvider serviceProvider = ServiceProvider.getInstance();
        final StudentService studentService = serviceProvider.getStudentService();

        LOGGER.info(studentService.getMarks("55", "History Of Music").toString());
    }

    }
