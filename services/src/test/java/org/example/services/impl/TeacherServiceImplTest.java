package org.example.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.services.ServiceProvider;
import org.example.services.TeacherService;
import org.junit.Test;

public class TeacherServiceImplTest  {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    @Test
    public void test() throws Exception {

        final ServiceProvider serviceProvider = ServiceProvider.getInstance();
        final TeacherService teacherService = serviceProvider.getTeacherService();

//        LOGGER.info("Student List: \n"+teacherService.getStudents("students").toString());
//
        LOGGER.info("List Students By Group: \n" + teacherService.getListStudentsByGroup("1313").toString());

        LOGGER.info("Groups: " + teacherService.getTeacherGroups("49"));
        System.out.println();
//        CommonUser commonUser = new CommonUser();
//        commonUser.setFirstName("Lenka");
//        commonUser.setLastName("Buslaya");
//        commonUser.setStudent(new Student("History Of Music", 6));
//        commonUser.setSubject(new Subject(Date.valueOf("2020-10-15")));

//        boolean result = teacherService.setMarks("Lenka", "Buslaya", "6", "2020-10-15" , "History Of Music");
//
//        LOGGER.info("Has mark been set? " + result);
    }
}
