package org.example.services.spring;

import org.example.services.AdminService;
import org.example.services.CommonUserService;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AdminServicesSpringTest {

    @Test
    public void testXml(){
        try(ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:service-test-context.xml")){
            final Object service = context.getBean("adminServiceImpl", AdminService.class);
            final Object service2 = context.getBean("commonUserServiceImpl", CommonUserService.class);

            ((AdminService)service).getUserById("55");
            ((CommonUserService)service2).authorize("qwe", "qwe");

            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAnnotation(){

        try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("org.example.services")){
            final AdminService adminService= context.getBean(AdminService.class);

//            adminService.getUserById("55");
            System.out.println();
        }
    }
}
