package org.example.services.impl;

import org.example.services.SessionCountService;

import java.util.concurrent.atomic.AtomicInteger;

public class SessionCountServiceImpl implements SessionCountService {

    private final AtomicInteger counter = new AtomicInteger(0);

    @Override
    public int count() {
        return counter.get();
    }

    @Override
    public int remove() {
        return counter.decrementAndGet();
    }

    @Override
    public int add() {
        return counter.incrementAndGet();
    }
}
