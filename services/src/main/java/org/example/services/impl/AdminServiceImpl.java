package org.example.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.db.dao.AdminDao;
import org.example.db.dao.DaoProvider;
import org.example.db.dao.spring.impl.AdminDaoImpl;
import org.example.model.CommonUser;
import org.example.model.UserRole;
import org.example.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    final DaoProvider daoProvider = DaoProvider.getInstance();
//    final AdminDao adminDao = daoProvider.getAdminDao();

    @Qualifier("adminDaoImpl")
    @Autowired
    private AdminDao adminDao;

    @Override
    public CommonUser createUser(String login, String password, String name, String surname, String role) {
        try{
            CommonUser user = new CommonUser();
            final int roleId;
            if (role.equals("student")) {
                roleId = 3;
                user.setUserRole(new UserRole(roleId));
            } else if (role.equals("teacher")) {
                roleId = 2;
                user.setUserRole(new UserRole(roleId));
            }
            user.setLogin(login);
            user.setPassword(password);
            user.setFirstName(name);
            user.setLastName(surname);

            LOGGER.info(user);
            boolean result = adminDao.createUser(user);
            if (result){
                return user;
            }else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteUserById(String id) throws Exception {
        Long longId = Long.parseLong(id);
        boolean result = adminDao.deleteUserById(longId);
        return result;
    }

    @Override
    public CommonUser getUserById(String id) throws Exception {
        Long longId = Long.parseLong(id);
        CommonUser commonUser = adminDao.getById(longId);
        return commonUser;
    }


}
