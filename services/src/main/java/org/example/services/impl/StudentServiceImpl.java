package org.example.services.impl;

import org.example.db.dao.DaoProvider;
import org.example.db.dao.StudentDao;
import org.example.db.dao.spring.impl.AdminDaoImpl;
import org.example.db.dao.spring.impl.StudentDaoImpl;
import org.example.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    final DaoProvider daoProvider = DaoProvider.getInstance();
//    final StudentDao studentDao = daoProvider.getStudentDao();

    @Qualifier("studentDaoImpl")
    @Autowired
    private StudentDao studentDao;

    @Override
    public List<Integer> getMarks(String id, String subjectName) throws Exception {

        Long student_id = Long.parseLong(id);

        List<Integer> list = studentDao.getMarks(student_id, subjectName);

        return list;
    }
}
