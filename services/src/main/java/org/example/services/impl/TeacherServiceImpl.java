package org.example.services.impl;

import com.google.protobuf.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.db.dao.*;
import org.example.db.dao.spring.impl.AdminDaoImpl;
import org.example.db.dao.spring.impl.TeacherDaoImpl;
import org.example.model.CommonUser;
import org.example.model.Marks;
import org.example.model.Subject;
import org.example.services.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);
    final DaoProvider daoProvider = DaoProvider.getInstance();
//    final TeacherDao teacherDao = daoProvider.getTeacherDao();

    @Qualifier("teacherDaoImpl")
    @Autowired
    private TeacherDao teacherDao;


    @Override
    public boolean setMarks(String name, String surname, String mark, String date , String subjectName) throws Exception {
        try {
            int markInt = Integer.parseInt(mark);

            CommonUser commonUser = new CommonUser();
            commonUser.setFirstName(name);
            commonUser.setLastName(surname);
            commonUser.setMarks(new Marks(markInt));
            commonUser.setSubject(new Subject(date, subjectName));

            final boolean result = teacherDao.ifMarkExist(commonUser);
            if(!result){
                return teacherDao.setMarks(commonUser);
            }else{
                return false;
            }

        } catch (Exception e) {
            LOGGER.error("service set mark");
            throw new ServiceException("service set mark", e);
        }
    }

    @Override
    public List<CommonUser> getListStudentsByGroup(String groupName) throws Exception {
        try{
            int numberOfGroup = Integer.parseInt(groupName);
            List<CommonUser> studentList = teacherDao.getStudentsFromGroup(numberOfGroup);
            System.out.println();
            return studentList;

        }catch (Exception e){
            LOGGER.error("Services. Teacher. Get ListStudentByGroup");
            throw new ServiceException("Services. Teacher. Get ListStudentByGroup");
        }


    }

    @Override
    public List<Integer> getTeacherGroups(String teacherId) throws Exception {
        Long id = Long.parseLong(teacherId);
        List<Integer> groups = teacherDao.getTeacherGroups(id);
        System.out.println();
        return groups;
    }


}
