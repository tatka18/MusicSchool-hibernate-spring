package org.example.services.impl;

import com.google.protobuf.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.db.dao.*;
import org.example.model.CommonUser;
import org.example.model.Subject;
import org.example.model.Teacher;
import org.example.model.UserRole;
import org.example.services.CommonUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@Service
public class CommonUserServiceImpl implements CommonUserService {
    final DaoProvider daoProvider = DaoProvider.getInstance();
//    final CommonUserDao commonUserDao = daoProvider.getCommonUserDao();

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    @Qualifier("commonUserDaoImpl")
    @Autowired
    private CommonUserDao commonUserDao;

    @Override
    public CommonUser authorize(String login, String password) throws ServiceException {

        CommonUser commonUser = new CommonUser(login, password);
        try {
            CommonUser fillUser = commonUserDao.authorization(commonUser);

                return fillUser;
        } catch (DaoException | SQLException | IOException | ClassNotFoundException e) {
            LOGGER.error("Error During Authorization Service");
            throw new ServiceException(e);
        }

    }

    @Override
    public boolean registration(String login, String password, String name, String surname, String role) throws ServiceException {

        try {
            CommonUser commonUser = new CommonUser();
            final int roleId;
            if (role.equals("student")) {
                roleId = 3;
                commonUser.setUserRole(new UserRole(roleId));
            } else if (role.equals("teacher")) {
                roleId = 2;
                commonUser.setUserRole(new UserRole(roleId));
            }

            commonUser.setLogin(login);
            commonUser.setPassword(password);
            commonUser.setFirstName(name);
            commonUser.setLastName(surname);

            LOGGER.info("----------------" + commonUser);

            return commonUserDao.registration(commonUser);

        } catch (DaoException | SQLException | IOException | ClassNotFoundException e) {
            LOGGER.error("registration service");
            throw new ServiceException("service registration", e);
        }
    }

    @Override
    public List<Teacher> getTeachers() throws Exception {
        try {
                List<Teacher> teacherList = commonUserDao.getTeacher();
                LOGGER.info(teacherList);
                return teacherList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Subject getSubjectInfo(String subjectName) throws Exception {
        return commonUserDao.getSubjectInfo(subjectName);
    }

    @Override
    public CommonUser getUser(String id) throws Exception {
        final Long idi = Long.parseLong(id);

        return commonUserDao.getUser(idi);
    }
}
