package org.example.services;

import org.example.services.impl.AdminServiceImpl;
import org.example.services.impl.CommonUserServiceImpl;
import org.example.services.impl.StudentServiceImpl;
import org.example.services.impl.TeacherServiceImpl;

public class ServiceProvider {
    private static final ServiceProvider instance = new ServiceProvider();

    private static CommonUserService commonUserService = new CommonUserServiceImpl();
    private static AdminService adminService = new AdminServiceImpl();
    private static StudentService studentService = new StudentServiceImpl();
    private static TeacherService teacherService = new TeacherServiceImpl();

    public static ServiceProvider getInstance() {
        return instance;
    }

    public CommonUserService getCommonUserService() {
        if (commonUserService == null) {
            synchronized (CommonUserService.class) {
                if (commonUserService == null) {
                    commonUserService = new CommonUserServiceImpl();
                }
            }
        }
        return commonUserService;
    }

    public AdminService getAdminService() {
        if (adminService == null) {
            synchronized (AdminService.class) {
                if (adminService == null) {
                    adminService = new AdminServiceImpl();
                }
            }
        }
        return adminService;
    }

    public StudentService getStudentService() {
            if (studentService == null) {
                synchronized (StudentService.class) {
                    if (studentService == null) {
                        studentService = new StudentServiceImpl();
                    }
                }
            }
            return studentService;
        }


    public TeacherService getTeacherService(){
        if (teacherService == null) {
            synchronized (TeacherService.class) {
                if (teacherService == null) {
                    teacherService = new TeacherServiceImpl();
                }
            }
        }
        return teacherService;
    }
}
