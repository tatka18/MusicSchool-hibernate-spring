package org.example.services;

import org.example.model.Student;

import java.util.List;

public interface StudentService {

    List<Integer> getMarks(String id, String subjectName) throws Exception;

}
