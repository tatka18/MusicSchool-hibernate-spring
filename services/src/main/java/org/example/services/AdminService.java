package org.example.services;

import org.example.model.CommonUser;

public interface AdminService {
    CommonUser createUser (String login, String password, String name, String surname, String role);
    boolean deleteUserById(String id) throws Exception;
    CommonUser getUserById(String id) throws Exception;



}
