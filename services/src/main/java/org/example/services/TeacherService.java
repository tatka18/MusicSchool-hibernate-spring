package org.example.services;

import org.example.model.CommonUser;

import java.util.List;

public interface TeacherService {
    boolean setMarks(String name, String surname, String mark, String date , String subjectName) throws Exception;
    List<CommonUser> getListStudentsByGroup(String groupNumber) throws Exception;
    List<Integer> getTeacherGroups(String teacherId) throws Exception;
}
