package org.example.services;

import com.google.protobuf.ServiceException;
import org.example.db.dao.DaoException;
import org.example.model.CommonUser;
import org.example.model.Subject;
import org.example.model.Teacher;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface CommonUserService {
    CommonUser authorize(String login, String password) throws ServiceException, ClassNotFoundException, SQLException, DaoException, IOException;
    boolean registration(String login, String password, String name, String surname, String role) throws ServiceException;
    List<Teacher> getTeachers() throws Exception;
    Subject getSubjectInfo(String subjectName) throws Exception;
    CommonUser getUser(String id) throws Exception;

}
