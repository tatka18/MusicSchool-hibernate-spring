<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    <%--взяли библиотеку--%>
<html>
<head>
    <title>User List</title>
    <style>
        table {width: 50%;
        border-collapse: collapse;
        border: 2px solid #ccc;
            margin-bottom: 2em;
        }
        table td{
            border: 2px solid #ccc;
            padding: 7px;
        }
        table th{
            border: 2px solid #ccc;
            padding: 7px;
        }
    </style>
</head>
<body style="background-color: rgba(82,53,53,0.15)">

<jsp:include page="groups_list.jsp"/>
<table>
    <tr>
        <th>№</th>
        <th>Surname</th>
        <th>Name</th>
<%--        <th>Mark</th>--%>
    </tr>

    <c:forEach items="${requestScope.student_list}" var="student" varStatus="pos">  <%--передаем значения--%>
        <tr><%--заполняем ячейки --%>
            <td>${pos.index + 1}</td>
            <td><c:out value="${student.lastName}" default="empty" /></td>
            <td><c:out value="${student.firstName}" default="empty" /></td>

        </tr>
    </c:forEach>

</table>

</body>
</html>
