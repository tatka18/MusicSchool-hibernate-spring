<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body style="background-color: rgba(82,53,53,0.15) ">

<jsp:include page="users.jsp"/>

<div align="center"><h1>${requestScope.result}</h1></div>

<c:if test="${requestScope.action.equals('create_user') == true}" >

<div align="center">
    <form action="create_user" method="post">
        <table style="with: 80%">

            <tr><p>Creating new user</p></tr>

            <tr>
                <td>Login:</td>
                <td><input name="login" type="text" /></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input name="password" type="text" /></td>
            </tr>
            <tr>
                <td>Name:</td>
                <td><input name="name" type="text" /></td>
            </tr>
            <tr>
                <td>Surname:</td>
                <td><input name="surname" type="text" /></td>
            </tr>
            <tr>
                <td>Role:</td>
                <td><select name="role">
                    <option>student</option>
                    <option>teacher</option>
                </select></td>
            </tr>
        </table>
        <input type="submit" value="create a new user"/>
    </form>
</div>
</c:if>

<c:if test="${requestScope.action.equals('get_by_id') == true}">

<div align="center">
    <form action="get_by_id" method="post">
         <table style="with: 80%">

             <tr><p>To find user by id</p></tr>

            <tr>
                <td>Id:</td>
                <td><input name="user_id" type="text" /></td>
            </tr>
        </table>
        <input type="submit" value="get user by id"/>
</form>
</div>
</c:if>


<c:if test="${requestScope.action.equals('delete') == true}">


<div align="center">
<form action="delete" method="post">
    <table style="with: 80%">

        <tr><p>Delete user by id</p></tr>

        <tr>
            <td>Id:</td>
            <td><input name="user_id" type="text" /></td>
        </tr>
    </table>
    <input type="submit" value="delete user by id"/>
</form>
</div>
</c:if>

</body>
</html>
