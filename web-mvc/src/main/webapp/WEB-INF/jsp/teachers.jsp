<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Tutor List</title>

    <style>
        table {width: 50%;
            border-collapse: collapse;
            border: 2px solid #ccc;
            margin-bottom: 2em;
        }
        table td{
            border: 2px solid #ccc;
            padding: 7px;
        }
        table th{
            border: 2px solid #ccc;
            padding: 7px;
        }
    </style>
</head>
<body style="background-color: rgba(82,53,53,0.15)">
<%--<jsp:include page="WEB-INF/components/header.jsp">--%>
<%--    <jsp:param name="disableMenu" value="true"/>--%>
<%--</jsp:include>--%>

<div align="center">
<table>
    <tr>
        <th>№</th>
        <th>Surname</th>
        <th>Name</th>
        <th>Subject</th>
    </tr>

    <c:forEach items="${requestScope.teacher_list}" var="teacher" varStatus="pos">  <%--передаем значения--%>
        <tr><%--заполняем ячейки --%>
            <td>${pos.index + 1}</td>
            <td><c:out value="${teacher.commonUser.lastName}" default="empty" /></td>
            <td><c:out value="${teacher.commonUser.firstName}" default="empty" /></td>

            <td>
                <form style="text-align: center">
                    <button formaction="subject" formmethod="get" name="subject_name" type="submit" value="${teacher.subject.subjectName}"
                            style="color: brown">
                        <c:out value="${teacher.subject.subjectName}" default="doesn't teach any subject"/>
                    </button>
                </form>
            </td>
        </tr>
    </c:forEach>

</table>

<%--&lt;%&ndash;Используем кнопку "Назад"&ndash;%&gt;--%>
<%--<input type="button" onclick="history.back();" value="Back" style="color: #402f30"/>--%>
</div>

<%--<jsp:include page="WEB-INF/components/footer.jsp">--%>
<%--    <jsp:param name="disableMenu" value="true"/>--%>
<%--</jsp:include>--%>

</body>
</html>
