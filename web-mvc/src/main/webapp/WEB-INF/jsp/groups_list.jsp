<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

<jsp:include page="users.jsp"/>
<div align="center">
    <table>
<c:forEach items="${requestScope.group_list}" var="group">

        <td>
            <form style="text-align: center">
                <button formaction="student_list" formmethod="get" name="group_number" type="submit" value="${group}"
                        style="color: brown">
                    <c:out value="${group}"/>
                </button>
            </form>
        </td>

</c:forEach>
    </table>
</div>

</body>
</html>
