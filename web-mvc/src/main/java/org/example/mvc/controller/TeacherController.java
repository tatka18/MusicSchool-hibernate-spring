package org.example.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TeacherController {

    @RequestMapping(path = "/teachers", method = RequestMethod.GET)
    public ModelAndView login(ModelAndView modelAndView){

        return modelAndView;
    }

    @RequestMapping(path = "/groups_list", method = RequestMethod.GET)
    public ModelAndView groupsList(ModelAndView modelAndView){

        return modelAndView;
    }
}
