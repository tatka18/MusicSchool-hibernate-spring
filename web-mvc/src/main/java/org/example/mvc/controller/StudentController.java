package org.example.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StudentController {

    @RequestMapping(path = "/marks", method = RequestMethod.GET)
    public ModelAndView studentMarks(ModelAndView modelAndView){

        return modelAndView;
    }
}
