package org.example.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Controller
public class WelcomeController {

    @RequestMapping(path = "/welcome", method = RequestMethod.GET)
    public ModelAndView welcome(ModelAndView modelAndView){
        final Set<String> names = new HashSet<>();
        names.add("Eric");
        names.add("Ivan");
        names.add("Hermes");

        modelAndView.addObject("usernames", names);
        modelAndView.addObject("current_date", LocalDate.now());

        return modelAndView;
    }
}
