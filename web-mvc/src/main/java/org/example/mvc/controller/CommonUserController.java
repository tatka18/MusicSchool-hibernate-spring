package org.example.mvc.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Controller
public class CommonUserController {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);
    private static final String USER_NAME_PARAM= "userName";
    private static final String USER_SURNAME_PARAM= "userSurname";
    private static final String USER_ROLE_ID_PARAM = "roleId";

    @RequestMapping(path = "/users", method = RequestMethod.GET)
    public ModelAndView welcome(ModelAndView modelAndView){
        final Set<String> names = new HashSet<>();
        names.add("Eric");
        names.add("Ivan");
        names.add("Hermes");

        modelAndView.addObject("usernames", names);
        modelAndView.addObject("current_date", LocalDate.now());

        return modelAndView;
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public ModelAndView login(ModelAndView modelAndView){


        return modelAndView;
    }

    @RequestMapping(path = "/logout", method = RequestMethod.GET)
    public ModelAndView logOut(ModelAndView modelAndView){
//        session.invalidate();
        return modelAndView;
    }

    @RequestMapping(path = "/register", method = RequestMethod.GET)
    public ModelAndView register(ModelAndView modelAndView){

        return modelAndView;
    }

    @RequestMapping(path = "/subject_info", method = RequestMethod.GET)
    public ModelAndView subjectInfo(ModelAndView modelAndView){

        return modelAndView;
    }
    @RequestMapping(path = "/teachers", method = RequestMethod.GET)
    public ModelAndView teachersList(ModelAndView modelAndView){

        return modelAndView;
    }

}
