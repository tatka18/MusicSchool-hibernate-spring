package org.example.mvc.util;

public interface WebConstants {
    String LOGIN_PARAM = "login";
    String PASSWORD_PARAM = "password";
    String FIRST_NAME_PARAM = "name";
    String LAST_NAME_PARAM = "surname";
    String ROLE_PARAM = "role";
    String ID_PARAM = "user_id";
    String PASSWORD_REPEAT_PARAM = "password_repeat";
    String LIST = "list";
//    String CONTROL_PASSWORD = "control_password";
    String ATTENDANCE_LIST = "attendance_list";
    String MARK = "mark";
    String DATE = "date";
    String SUBJECT_CHOOSE = "subject_list";
    String LOGOUT_YES = "yes";
    String ADMIN_CREATE_USER = "create";
    String ADMIN_DELETE_USER = "delete";
    String ADMIN_ACTION = "action";
    String SUBJECT_NAME = "subject_name";
    String GROUP_NUMBER = "group_number";
    String SET_MAKS_BOTTOM = "set+marks";



}
