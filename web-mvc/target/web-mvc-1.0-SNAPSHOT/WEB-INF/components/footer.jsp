<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style><%@include file="../css/styles.css"%></style>

<body>

<div class="footer">

    <nav>
        <h5>
            <c:if test="${param.disableMenu != 'true'}">
                    <a href="login.jsp">LogIn</a> |
                    <a href="register.jsp">Registration</a> |
            </c:if>

        <a href="index.jsp">Home</a> |
            <c:if test="${param.disableMenu == 'true'}">
                <a href="logout.jsp">LogOut</a> |
            </c:if>

            <a href="https://www.google.com/?hl=ru" target="blank">Google</a> |
            <a href="mailto:tatka19871218@gmail.com" target="blank">Mail to us</a><br/>

        </h5>
    </nav>
   <h6>
       <span>
           2020 * Music School named by Shagov Anatoli.
           <address>Nonamed Street, building 1</address>
       </span>
   </h6>
</div>
</body>

