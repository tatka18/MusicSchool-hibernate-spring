<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Subject Info</title>
</head>
<body style="background-color: rgba(82,53,53,0.15)">
<%--<jsp:include page="WEB-INF/components/header.jsp">--%>
<%--    <jsp:param name="disableMenu" value="true"/>--%>
<%--</jsp:include>--%>

<br/>

<fieldset>
<table style="background: floralwhite" width="75%" align="center" >
    <tr>
        <th>
            <div align="center"><h4>${requestScope.subject_info}</h4></div>
        </th>
    </tr>
</table>
</fieldset>
<%--Используем кнопку "Назад"--%>
<input type="button" onclick="history.back();" value="Back" style="color: #402f30"/>

<%--<jsp:include page="WEB-INF/components/footer.jsp">--%>
<%--    <jsp:param name="disableMenu" value="true"/>--%>
<%--</jsp:include>--%>
</body>
</html>
