package org.example.db.connection;

import org.example.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;

import javax.persistence.EntityManager;

public class HibernateTest {
    @Test
    public void test(){
       final EntityManager em = HibernateUtil.getEntityManager();
       em.getTransaction().begin();
       final CommonUser commonUser = new CommonUser("koba", "kobochka", "Dokonki", "Hokot");
       em.persist(commonUser); //заполняем значением таблицу базы данных
       em.getTransaction().commit();

        final CommonUser commonUser1 = em.find(CommonUser.class, 1L);

       em.close();
       HibernateUtil.close();
    }
    @Test
    public void testSessionFactory(){
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try(final Session session = sessionFactory.openSession()){
            session.getTransaction().begin();

            final CommonUser commonUser = new CommonUser("Hna", "yjk", "dyka", "pura");
            session.persist(commonUser); //заполняем значением таблицу базы данных
            session.getTransaction().commit();
        }
        HibernateUtil.closeSF();
    }

    @Test
    public void initHibernate() {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            final CommonUser commonUser = new CommonUser("Hauna", "hyujk", "dhyuka", "puhura");
            session.persist(commonUser);//заполняем значением таблицу базы данных

            final UserRole role = new UserRole("student" );
            session.persist(role);//заполняем значением таблицу базы данных

            final Student student = new Student(new Marks(6));
            session.persist(student);//заполняем значением таблицу базы данных

            final Subject subject = new Subject(1L, "Vocal","Bla, bla, bla, bla", new Dates(java.sql.Date.valueOf("2020-10-16")));
            session.persist(subject);//заполняем значением таблицу базы данных

            session.getTransaction().commit();
        }
            sessionFactory.close();

    }
    @Test
    public void initHibernate2() {
        HibernateUtil.getSessionFactory().close();
    }

}
