package org.example.db.hibernate;

import org.example.db.connection.HibernateUtil;
import org.example.model.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class TeacherDaoTest extends HibernateAbstractTest {

    @Test
    public void setMark() {
        try (final Session session = getSessionFactory().openSession()) {
            try {
                final EntityManagerFactory emf = HibernateUtil.getEntityManager().getEntityManagerFactory();
                EntityManager em = emf.createEntityManager();



                em.getTransaction().begin();
                Query<?> setMark = session
                        .createNativeQuery("INSERT INTO marks (student_id, subject_id, day_id, mark) " +
                                "VALUES(SELECT u.id FROM users u WHERE first_name=?3 AND last_name=?4)," +
                                "(SELECT s.id FROM subjects s WHERE subject_name=?5), " +
                                "(SELECT d.id FROM dates d WHERE dates=?2), ?1)");
                        setMark.setParameter(1, 3);
                        setMark.setParameter(2, Date.valueOf("2020-09-02"));
                        setMark.setParameter(3, "Kostik");
                        setMark.setParameter(4, "Levonchik");
                        setMark.setParameter(5, "History Of Music");

List<String> o = (List<String>)setMark.getSingleResult();
                System.out.println(setMark);
setMark.getResultList();
                System.out.println();
//
//               em.persist(o);
                em.getTransaction().commit();
                System.out.println();

                System.out.println();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testSetMarks() {
        try (final Session currentSession = getSessionFactory().openSession()) {
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<CommonUser> query = criteriaBuilder.createQuery(CommonUser.class);
            final Root<CommonUser> root = query.from(CommonUser.class);

            query.select(root)
                    .where(criteriaBuilder.equal(root.get("firstName"), "Maka"),
                            (criteriaBuilder.equal(root.get("lastName"), "Taka")));
            Long studentId = currentSession.createQuery(query).getSingleResult().getId();

            final CriteriaQuery<Subject> query2 = criteriaBuilder.createQuery(Subject.class);
            final Root<Subject> root2 = query2.from(Subject.class);
            query2.select(root2)
                    .where(criteriaBuilder.equal(root2.get("subjectName"), "History Of Music"));
            Long subjectId = currentSession.createQuery(query2).getSingleResult().getId();

            final CriteriaQuery<Dates> query3 = criteriaBuilder.createQuery(Dates.class);
            final Root<Dates> root3 = query3.from(Dates.class);
            query3.select(root3)
                    .where(criteriaBuilder.equal(root3.get("date"), Date.valueOf("2020-09-02")));
            Long dateId = currentSession.createQuery(query3).getSingleResult().getId();


            System.out.println(studentId + " " + subjectId + " " + dateId);

        }
    }

    @Test
    public void testSetMarksWithJoin() {
        try (final Session currentSession = getSessionFactory().openSession()) {
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<CommonUser> query = criteriaBuilder.createQuery(CommonUser.class);
            final Root<CommonUser> root = query.from(CommonUser.class);

            final Join<Object, Object> oo =  root.join("userRole", JoinType.LEFT);
            query.select(root);

            criteriaBuilder.parameter(Boolean.class);

            final List<CommonUser> list = currentSession.createQuery(query).getResultList();
            System.out.println();

        }
    }
}
