package org.example.db.hibernate;

import org.example.db.connection.HibernateUtil;
import org.example.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class HibernateAbstractTest {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @BeforeClass
    public static void beforeClass(){
        sessionFactory = HibernateUtil.getSessionFactory();
        prepareDb();

    }

    private static void prepareDb() {
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();

            final List<UserRole> userRoleList = new ArrayList<>();  //добавлены две сущности
            userRoleList.add(new UserRole("teacher"));
            userRoleList.add(new UserRole("student"));

//            userRoleList.forEach(session::persist);

            final List<CommonUser> commonUserList = new ArrayList<>();        //добавлен список контактов
            commonUserList.add(new CommonUser("fool", "cbmkc", "Leba", "Croom"));
            commonUserList.add(new CommonUser("poolll", "srttttj", "Dooka", "Soob"));
            commonUserList.add(new CommonUser("toot", "gfdgsdg", "Melok", "Joinnik"));
            commonUserList.add(new CommonUser("poop", "gjjdart", "John", "Dubovik"));
            commonUserList.add(new CommonUser("cool", "fgjsfg", null, "Luchina"));
            commonUserList.add(new CommonUser("soon", "hmgcs", "User", "Blockina"));
            commonUserList.add(new CommonUser("doom", "hmgcs", null, "Adminkina"));

            for (int i = 0; i < commonUserList.size(); i++ ) { //условное разделение контактов по ролям
                commonUserList.get(i).setUserRole(userRoleList.get(i % 2));
            }
//            commonUserList.forEach(session::persist);

            final Teacher teacher = new Teacher();
            teacher.setCommonUser(commonUserList.get(0));
//            session.persist(teacher);

            final Student student = new Student();
            student.setCommonUser(commonUserList.get(1));
//            session.persist(student);

            final Subject subject = new Subject("Vocal", "Created special for students who want to be a professional singer.");
//            session.persist(subject);

            final List<Dates> date = new ArrayList<>();
            date.add(new Dates(Date.valueOf("2020-10-10")));
            date.add(new Dates(Date.valueOf("2020-10-11")));
            date.add(new Dates(Date.valueOf("2020-10-12")));
//            date.forEach(session::persist);

            final List<Group> groupList = new ArrayList<>();
            groupList.add(new Group(1234));
            groupList.add(new Group(3454));
            groupList.add(new Group(5678));
            groupList.add(new Group(5456));
//            groupList.forEach(session::persist);

            final Teacher teacherGroup = new Teacher();
            teacherGroup.addGroup(groupList.get(0));
            teacherGroup.setCommonUser(commonUserList.get(2));
//            session.persist(teacherGroup);

            final Teacher teacherSubject = new Teacher();
            teacherSubject.setCommonUser(commonUserList.get(4));
            teacherSubject.setSubject(subject);
//            session.persist(teacherSubject);

            final Student studentGroup = new Student();
            studentGroup.setGroup(groupList.get(1));
            studentGroup.setCommonUser(commonUserList.get(3));
//            session.persist(studentGroup);

            final Marks marks = new Marks();
            marks.setMark(3);
            marks.setDates(date.get(1));
            marks.setSubject(subject);
            marks.setStudent(student);
//            session.persist(marks);

            transaction.commit();
        }
    }

    @AfterClass
    public static void afterClass(){
        sessionFactory.close();
    }
}
