package org.example.db.hibernate;

import org.example.model.CommonUser;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class JpqlHqlTest extends HibernateAbstractTest {

    @Test
    public void testFrom(){
        final Session session = getSessionFactory().openSession();
        final Query<CommonUser> from_user = session.createQuery("from CommonUser", CommonUser.class);
        final List<CommonUser> resultList  = from_user.getResultList();
        resultList.forEach(commonUser ->
                System.out.println(String.format("%s %s %s %s", commonUser.getFirstName(), commonUser.getLastName(), commonUser.getLogin(), commonUser.getPassword()))
        );
    }

    @Test
    public void testFromAs(){
        final Session session = getSessionFactory().openSession();
        final Query<CommonUser> from_user = session.createQuery("from CommonUser AS c");
        final List<CommonUser> resultList  = from_user.getResultList();
        resultList.forEach(commonUser ->
                System.out.println(String.format("%s %s %s %s", commonUser.getFirstName(), commonUser.getLastName(), commonUser.getLogin(), commonUser.getPassword())));
    }

    @Test
    public void testSelectFromAs(){
        final Session session = getSessionFactory().openSession();
        final Query<String> from_user = session.createQuery("select c.lastName from CommonUser AS c", String.class);
        final List<String> resultList  = from_user.getResultList();
        resultList.forEach(u ->System.out.println(u));
    //(s ->System.out.println(s)); лямбда. Если мы не используем лямбду, то нам нужно использовать анонимный класс
    }
//
//    @Test
//    public void testSelectFromAsWithDiffObject(){
//        final Session session = getSessionFactory().openSession();
//        final Query<UserInfo> from_contact = session.createQuery("select c.firstName, c.lastName from Contact AS c")
//                .setResultTransformer(Transformers.aliasToBean(UserInfo.class));
//        final List<UserInfo> resultList  = from_contact.getResultList();
//        resultList.forEach(contact->
//                System.out.println(String.format("%s  ---  %s", contact.getFirstName(), contact.getLastName())));
//    }

    @Test
    public void testSelectFromGroupBy(){
        final Session session = getSessionFactory().openSession();
        final Query<?> from_user = session.createQuery("select c.password, count (c.password) from CommonUser AS c group by c.password");

        final List<Object[]> resultList  = (List<Object[]>)from_user.getResultList();

        resultList.forEach(u ->
        System.out.println(String.format("%s  ---  %s", u[0], u[1])));
    }

    @Test
    public void testSelectFromGroupByWhere(){
        final Session session = getSessionFactory().openSession();
        final Query<?> from_user = session.createQuery("select c.password, count (c.password) from CommonUser AS c where c.password not like 's%' group by c.password");

        final List<Object[]> resultList  = (List<Object[]>)from_user.getResultList();

        resultList.forEach(u ->
                System.out.println(String.format("%s  ---  %s", u[0], u[1])));
    }

    @Test
    public void testSelectFromHaving(){
        final Session session = getSessionFactory().openSession();
        final Query<?> from_user = session.createQuery("select u.password, count (u.password) from CommonUser u where u.password " +
                "not like 's%' group by u.password having count (u.password) > 1");

        final List<Object[]> resultList  = (List<Object[]>)from_user.getResultList();

        System.out.println("testSelectFromHaving--------------------------");

        resultList.forEach(t ->
                System.out.println(String.format("%s  ---  %s", t[0], t[1])));
    }

    @Test
    public void testSelectFromHavingOrderBy(){
        final Session session = getSessionFactory().openSession();
        final Query<?> from_user = session.createQuery("select c.password, count (c.password) from CommonUser AS c where c.password " +
                "not like 's%' group by c.password having count (c.password) > 3 order by count (c.password) desc");

        final List<Object[]> resultList  = (List<Object[]>)from_user.getResultList();

        System.out.println("testSelectFromHavingOrderBy--------------------------");

        resultList.forEach(t ->
                System.out.println(String.format("%s  ---  %s", t[0], t[1])));
    }

    @Test
    public void testUpdate(){   //функциональность, которую поддерживает хибернейт
        final Session session = getSessionFactory().openSession();
        final Transaction transaction = session.beginTransaction();
        final Query<?> from_user = session
                .createQuery("update CommonUser set password = ?1 where lastName = :name")
                .setParameter(1, "pass_changed")
                .setParameter("name", "Soob");
        final int i = from_user.executeUpdate();
        System.out.println(i);
        transaction.commit();
    }

    @Test
    public void testUpdateWithListParam(){
        final Session session = getSessionFactory().openSession();
        final Transaction transaction = session.beginTransaction();
        final Query<?> from_user = session
                .createQuery("update CommonUser set password = ?1 where id in (?2)")
                .setParameter(1, "pass_changed")
                .setParameter(2, Arrays.asList(3L, 6L, 5L));
        final int i = from_user.executeUpdate();
        System.out.println("----------------------" + i);
        transaction.commit();
    }

    @Test
    public void testInsert(){
        final Session session = getSessionFactory().openSession();
        final Transaction transaction = session.beginTransaction();
        final Query<?> from_user = session.createQuery("insert into CommonUser (login, password, firstName, lastName) " +
                "select concat( u2.login, '_test'), u2.password, u2.firstName, u2.lastName from CommonUser u2");

        final int i = from_user.executeUpdate();
        System.out.println(i);
        transaction.commit();
    }

    @Test
    public void testFromPagination() {
        int page = 1;
        int pageSize = 3;
        final Session session = getSessionFactory().openSession();
        final Query<CommonUser> from_user = session.createQuery("from CommonUser", CommonUser.class)
                .setFirstResult((page - 1) * pageSize) // 3    (0, 1, 2, 3, 4, 5 ...) считается с 0
                .setMaxResults(pageSize); // 3

        final List<CommonUser> resultList = from_user.getResultList();
        resultList.forEach(user ->
                System.out.println(String.format("%s %s %s %s", user.getFirstName(), user.getLastName(), user.getLogin(), user.getPassword())));
    }
}
