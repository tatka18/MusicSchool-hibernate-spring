package org.example.db.hibernate;

import org.example.model.CommonUser;
import org.example.model.Teacher;
import org.example.model.UserRole;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.Test;

import java.io.Serializable;

import java.util.List;

public class CommonUserDaoTest extends HibernateAbstractTest{


    @Test
    public void authorization() {
        final Session session = getSessionFactory().openSession();

        CommonUser commonUser = new CommonUser("ru", "ru");
        Query<?> authoriz = session.createQuery(
                "SELECT u.firstName, u.lastName, u.id, u.userRole.id FROM CommonUser u WHERE password=?1 AND login=?2")
                .setParameter(1, commonUser.getPassword())
                .setParameter(2, commonUser.getLogin());

        final List<Object[]> resultList = (List<Object[]>) authoriz.getResultList();

//        System.out.println(resultList.size());
        resultList.forEach(u ->
                System.out.println(String.format("%s %s --- %s---%s", u[0], u[1], u[2], u[3]) +"-----------------"));
    }

    @Test
    public void registration() {
        CommonUser user = new CommonUser("spoon","moon", null, null, new UserRole(2));

        try(final Session session = getSessionFactory().openSession()){
            session.getTransaction().begin();
            Serializable result = session.save(user);
            session.getTransaction().commit();

            if(result != null){
                System.out.println("successfully ---id = " + result.toString());
            }else {
                System.out.println("error");
            }
        }
    }

    @Test
    public void getTeacher() {
        final Session session = getSessionFactory().openSession();

        session.getTransaction().begin();
        Query<CommonUser> list = session.createQuery("FROM CommonUser u WHERE userRole.name='teacher'", CommonUser.class);
        List<CommonUser> commonUser = list.getResultList();
        session.getTransaction().commit();
        commonUser.forEach(user ->
                System.out.println(String.format("%s %s %s %s", user.getFirstName(), user.getLastName(), user.getLogin(), user.getPassword()))
        );
    }

    @Test
    public void getSubjectInfo(){
        try(final Session session  = getSessionFactory().openSession()) {
            try {
                session.getTransaction().begin();
                Query<Teacher> list = session.createQuery(" FROM Subject u where u.subjectName='Vocal'", Teacher.class);
                List<Teacher> teachers = list.getResultList();

                session.getTransaction().commit();
                teachers.forEach(user ->
                        System.out.println(String.format("%s", user.getSubject().getSpecification()))
                );

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
