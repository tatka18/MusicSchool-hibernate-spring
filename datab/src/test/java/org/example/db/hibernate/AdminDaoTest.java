package org.example.db.hibernate;

import org.example.model.CommonUser;
import org.hibernate.Session;
import org.junit.Test;

import java.io.Serializable;

public class AdminDaoTest extends HibernateAbstractTest{

    @Test
    public void createUser(){
        CommonUser commonUser = new CommonUser("loooon", "moon");
        try (final Session session = getSessionFactory().openSession()) {
            session.getTransaction().begin();
            final Serializable save = session.save(commonUser);
            session.getTransaction().commit();
            System.out.println(save.toString() + "----id");
        }
    }

    @Test
    public void getById(){
        try (final Session session = getSessionFactory().openSession()) {
            Long id = 55L;
            session.getTransaction().begin();
            CommonUser commonUser = session.get(CommonUser.class, id);
            session.getTransaction().commit();

            System.out.println("byId ---" +commonUser.getFirstName() + " " + commonUser.getLastName());
        }
    }

    @Test
    public void deleteUserById(){
        try (final Session session = getSessionFactory().openSession()) {
            Long id=4L;
            session.getTransaction().begin();
            CommonUser commonUser = session.get(CommonUser.class, id);
            session.delete(commonUser);
            session.getTransaction().commit();
            CommonUser i = session.get(CommonUser.class, id);
            if(i==null){
                System.out.println("deleted");
            }else {
                System.out.println("error during deleting by id");
            }

        }
    }
}
