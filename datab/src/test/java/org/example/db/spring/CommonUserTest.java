package org.example.db.spring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.db.repository.CommonUserRepository;
import org.example.db.repository.SubjectRepository;
import org.example.db.repository.TeacherRepository;
import org.example.model.CommonUser;
import org.example.model.Teacher;
import org.example.model.UserRole;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring-data-context.xml")
public class CommonUserTest {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    @Autowired
    private CommonUserRepository commonUserRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Test
    public void registration(){
        CommonUser c = new CommonUser("bu", "mu", "Shka", "Dakkarta");
        commonUserRepository.save(c);

        c.setUserRole(new UserRole(2));
        commonUserRepository.save(c);

        LOGGER.info(c.toString());
    }

    @Test
    public void authorization(){
        CommonUser commonUser = new CommonUser("qwe", "qwe");
        LOGGER.info( commonUserRepository.findByLoginAndPassword(commonUser.getLogin(), commonUser.getPassword()));
    }

    @Test
    public void getTeacher(){
        subjectRepository.findAll();
        List<Teacher> list = teacherRepository.findAll();

LOGGER.info(list.get(2).getCommonUser().getPassword());
        LOGGER.info(list.get(2).getCommonUser().getLastName());
        LOGGER.info(list.get(2).getCommonUser().getSubject().getSubjectName());
//        System.out.println();

    }


    @Test
    public void getSubjectInfo(){

        LOGGER.info(subjectRepository.findBySubjectName("Vocal").getSpecification());
    }


}
