package org.example.db.spring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.db.repository.CommonUserRepository;
import org.example.model.CommonUser;
import org.example.model.UserRole;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring-data-context.xml")
public class AdminTest {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    @Autowired
    private CommonUserRepository commonUserRepository;

    @Test
    public void createUser(){
        CommonUser c = new CommonUser("ti", "tu", "Shka", "Dakkarta");
        commonUserRepository.save(c);

        c.setUserRole(new UserRole(3));
        commonUserRepository.save(c);

       LOGGER.info(c.toString());
    }

    @Test
    public void findById() {
        LOGGER.info(commonUserRepository.findById(55L).orElse(null));
    }

    @Test
    public void deleteById() {
        commonUserRepository.deleteById(84L);
        LOGGER.info(commonUserRepository.existsById(84L));
    }

}
