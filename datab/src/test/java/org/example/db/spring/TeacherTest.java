package org.example.db.spring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.db.repository.TeacherRepository;
import org.example.model.CommonUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.aop.AopInvocationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring-data-context.xml")
public class TeacherTest {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    @Autowired
    private TeacherRepository teacherRepository;

    @Test
    public void getStudentsFromGroup(){
        List<Object[]> list = teacherRepository.getStudentsFromGroup(1313);
        List<CommonUser> commonUser = new ArrayList<>();
        for(Object[] results:list) {
            CommonUser user = new CommonUser();
            user.setFirstName((String)results[0]);
            user.setLastName((String)results[1]);
            commonUser.add(user);
        }

        LOGGER.info(commonUser.toString());
    }
    @Test
    public void getTeacherGroups(){
        List<Integer> list = teacherRepository.getGroups(49L);

        LOGGER.info(list.toString());
    }

    @Test
    public void existMark(){
        try {
            int g = teacherRepository.ifMarkExist("Kostik", "Levonchik", "Conduction",(Date.valueOf("2020-09-02")));
            LOGGER.info(g);
        }catch (AopInvocationException e){
            LOGGER.info("null");
        }
    }

    @Test
    public void setMark(){

    }

}
