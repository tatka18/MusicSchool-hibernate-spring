package org.example.db.spring;

import org.example.db.repository.CommonUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring-data-context.xml")
public class SpringTest {

    @Autowired
    private CommonUserRepository commonUserRepository;

    @Test
    public void test(){
        final long count = commonUserRepository.count();
        System.out.println(count);
    }
}
