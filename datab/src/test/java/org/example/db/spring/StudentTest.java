package org.example.db.spring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.db.repository.StudentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring-data-context.xml")
public class StudentTest {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    @Autowired
    StudentRepository studentRepository;
    @Test
    public void getMarks(){
        List<Integer> list = studentRepository.getMarks(55L, "History Of Music");

        LOGGER.info(list.toString());
    }
}
