package org.example.db.repository;

import org.example.model.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

    @Query(value = "select m.mark from Marks m where m.student.id=?1 and m.subject.id=(select s.id from Subject s where s.subjectName=?2)")
    List<Integer> getMarks(Long studentId, String subjectName);
}
