package org.example.db.repository;

import org.example.model.Teacher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface TeacherRepository extends CrudRepository<Teacher, Long> {

    @Query(value = "select u.firstName, u.lastName from CommonUser u where u.id=u.student.id and u.student.group.id=(select g.id from Group g where g.groupNumber=?1)")
    List<Object[]> getStudentsFromGroup(int groupNumber);

    @Query(value = "select g.groupNumber from Group g join g.teachers tg where tg.id= ?1")
    List<Integer> getGroups(Long teacherId);

    @Query(value = "select m.mark from Marks m where " +
            "m.student.id=(select u.id from CommonUser u where u.firstName=?1 and u.lastName=?2) and " +
            "m.subject.id=(select s.id from Subject s where s.subjectName=?3) and " +
            "m.dates.id=(select d.id from Dates d where d.date=?4)")
    int ifMarkExist(String firstName, String lastName, String subjectName, Date date);

    @Override
    List<Teacher> findAll();


//not in use
    @Query(value = "select s.id from Subject s where s.subjectName=?1")
    Long findSubjectIdBySubjectName(String subjectName);

    @Query(value = "select d.id from Dates d where d.date=?1")
    Long findDateIdByDate(Date date);

    @Query(value = "select c.id from CommonUser c where c.firstName=?1 and c.lastName=?2")
    Long findUserIdByNameAndSurname(String name, String surname);

}
