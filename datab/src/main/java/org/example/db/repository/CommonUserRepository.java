package org.example.db.repository;

import org.example.model.CommonUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;

@Repository
public interface CommonUserRepository extends CrudRepository<CommonUser, Long> {

    CommonUser findByLoginAndPassword(String login, String password);

}
