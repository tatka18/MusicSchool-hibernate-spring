package org.example.db.repository;

import org.example.model.CommonUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends CrudRepository<CommonUser, Long> {

    CommonUser countByFirstName(String name);
    

}
