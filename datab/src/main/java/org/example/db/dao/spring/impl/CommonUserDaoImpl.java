package org.example.db.dao.spring.impl;

import org.example.db.dao.CommonUserDao;
import org.example.db.repository.CommonUserRepository;
import org.example.db.repository.SubjectRepository;
import org.example.db.repository.TeacherRepository;
import org.example.model.CommonUser;
import org.example.model.Subject;
import org.example.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonUserDaoImpl implements CommonUserDao {

    @Autowired
    private CommonUserRepository commonUserRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Override
    public CommonUser getUser(Long id){
        return commonUserRepository.findById(id).orElse(null);
    }

    @Override
    public CommonUser authorization(CommonUser commonUser){
        return commonUserRepository.findByLoginAndPassword(commonUser.getLogin(), commonUser.getPassword());
    }

    @Override
    public boolean registration(CommonUser commonUser){
        CommonUser c = new CommonUser(
                commonUser.getLogin(),
                commonUser.getPassword(),
                commonUser.getFirstName(),
                commonUser.getLastName());
        commonUserRepository.save(c);

        c.setUserRole(commonUser.getUserRole());
        Long rez = commonUserRepository.save(c).getId();
        return rez != null;
    }

    @Override
    public List<Teacher> getTeacher() {
subjectRepository.findAll();
        return teacherRepository.findAll();
    }

    @Override
    public Subject getSubjectInfo(String subjectName){
        subjectRepository.findBySubjectName(subjectName);
        return null;
    }
}
