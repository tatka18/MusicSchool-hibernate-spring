package org.example.db.dao.hibernate.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.HibernateUtil;
import org.example.db.connection.JdbcProvider;
import org.example.db.dao.CommonUserDao;
import org.example.model.CommonUser;
import org.example.model.Subject;
import org.example.model.Teacher;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class CommonUserDaoImpl implements CommonUserDao {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);
    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public CommonUser getUser(Long id) throws Exception {
        try(Session session = sessionFactory.openSession()){
            return  session.get(CommonUser.class, id);
        }
    }

    @Override
    public CommonUser authorization(CommonUser commonUser) {
        try (Session session = sessionFactory.openSession()) {
            try {
                Query<CommonUser> userQuery = session.createQuery(
                        "FROM CommonUser u WHERE password=?1 AND login=?2", CommonUser.class)
                        .setParameter(1, commonUser.getPassword())
                        .setParameter(2, commonUser.getLogin());

                if (userQuery.getResultList().size() != 0) {
                    return userQuery.getSingleResult();
                }
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            } finally {
                session.close();
            }
        }return null;
    }

    @Override
    public boolean registration(CommonUser user) {
        try(Session session = sessionFactory.openSession()){
            try {
                session.getTransaction().begin();
                final Serializable result = session.save(user);
                session.getTransaction().commit();

                if (result != null) {
                    return true;
                }
          } catch (HibernateException e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }finally {
            session.close();
            }
        }
        return false;
    }


    @Override
    public List<Teacher> getTeacher(){

        try(final Session session  = sessionFactory.openSession()) {
            try {
                session.getTransaction().begin();
                Query<Teacher> list = session.createQuery(" FROM Teacher u", Teacher.class);
                List<Teacher> teachers = list.getResultList();

        return teachers;
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            }finally {
                session.close();
            }
        }
        return null;
    }

    @Override
    public Subject getSubjectInfo(String subjectName){
        try (final Session session = sessionFactory.openSession()) {
            try{
            Query<Subject> query = session.createQuery("from Subject s where subjectName=?1", Subject.class)
                    .setParameter(1, subjectName);

                return query.getSingleResult();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            }finally {
                session.close();
            }
        }return null;
    }

    private CommonUser fillUser(ResultSet resultSet) throws SQLException{
        final CommonUser user = new CommonUser();
        user.setId(resultSet.getLong("id"));
        user.setLogin(resultSet.getString("login"));
        user.setPassword(resultSet.getString("password"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.getUserRole().setId(resultSet.getInt("role_id"));
        return user;
    }

    private CommonUser userSingle(ResultSet resultSet) throws SQLException{
        final CommonUser user = new CommonUser();
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));

        return user;
    }

    private CommonUser fillTeacher(ResultSet resultSet) throws SQLException{
        final CommonUser user = new CommonUser();
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setSubject(new Subject(resultSet.getString("subject_name")));
        LOGGER.info(resultSet.getString("first_name"));
        LOGGER.info(resultSet.getString("last_name"));
        LOGGER.info(resultSet.getString("subject_name"));
        return user;
    }

}
