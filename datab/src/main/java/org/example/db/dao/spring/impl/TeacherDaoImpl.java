package org.example.db.dao.spring.impl;

import org.example.db.dao.TeacherDao;
import org.example.db.repository.TeacherRepository;
import org.example.model.CommonUser;
import org.springframework.aop.AopInvocationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeacherDaoImpl implements TeacherDao {

    @Autowired
    private TeacherRepository teacherRepository;

    @Override
    public List<CommonUser> getStudentsFromGroup(int groupNumber){
        List<Object[]> list = teacherRepository.getStudentsFromGroup(groupNumber);
        List<CommonUser> commonUser = new ArrayList<>();
        for(Object[] results:list) {
            CommonUser user = new CommonUser();
            user.setFirstName((String)results[0]);
            user.setLastName((String)results[1]);
            commonUser.add(user);
        }
        return commonUser;
    }

    @Override
    public List<Integer> getTeacherGroups(Long teacherId){
        return teacherRepository.getGroups(teacherId);
    }

    @Override
    public boolean ifMarkExist(CommonUser commonUser){
        try {
            teacherRepository.ifMarkExist(
                    commonUser.getFirstName(),
                    commonUser.getLastName(),
                    commonUser.getSubject().getSubjectName(),
                    commonUser.getDates().getDate());
            return true;
        }catch (AopInvocationException e){
            return false;
        }
    }

    @Override
    public boolean setMarks(CommonUser commonUser){
        return false;
    }



    @Override
    public boolean set_attendance(CommonUser commonUser){
        return false;
    }

    @Override
    public void setNotesForStudent() {

    }
}
