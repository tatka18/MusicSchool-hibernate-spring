package org.example.db.dao.spring.impl;

import org.example.db.dao.AdminDao;
import org.example.db.repository.AdminRepository;
import org.example.db.repository.CommonUserRepository;
import org.example.model.CommonUser;
import org.example.model.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminDaoImpl implements AdminDao {

    @Autowired
    private CommonUserRepository commonUserRepository;

    @Override
    public boolean createUser(CommonUser commonUser){
        CommonUser c = new CommonUser(
                commonUser.getLogin(),
                commonUser.getPassword(),
                commonUser.getFirstName(),
                commonUser.getLastName());
        commonUserRepository.save(c);

        c.setUserRole(commonUser.getUserRole());
        Long rez = commonUserRepository.save(c).getId();
        return rez != null;
    }

    @Override
    public CommonUser getById(Long id){
        return commonUserRepository.findById(id).orElse(null);
    }

    @Override
    public boolean deleteUserById(Long id){
        commonUserRepository.deleteById(id);
        return commonUserRepository.existsById(id);
    }

    @Override
    public CommonUser getByLogin(String login) throws Exception {
        return null;
    }

    @Override
    public void updateUser() {

    }

    @Override
    public void getInfoAboutUser() {

    }

    @Override
    public void deleteUserByName(String firstName, String lastName) throws Exception {

    }
}
