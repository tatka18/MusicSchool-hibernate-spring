package org.example.db.dao;

import org.example.model.CommonUser;
import org.example.model.Subject;
import org.example.model.Teacher;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface CommonUserDao {
    CommonUser getUser(Long id) throws Exception;
    CommonUser authorization(CommonUser commonUser) throws DaoException, SQLException, IOException, ClassNotFoundException;
    boolean registration(CommonUser user) throws DaoException, SQLException, IOException, ClassNotFoundException;
    List<Teacher> getTeacher() throws Exception;
    Subject getSubjectInfo(String subjectName) throws SQLException, IOException, ClassNotFoundException;
}
