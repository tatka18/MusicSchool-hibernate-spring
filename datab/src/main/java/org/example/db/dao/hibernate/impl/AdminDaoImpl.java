package org.example.db.dao.hibernate.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.HibernateUtil;
import org.example.db.connection.JdbcProvider;
import org.example.db.dao.AdminDao;
import org.example.model.CommonUser;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AdminDaoImpl implements AdminDao {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);
    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public boolean createUser(CommonUser commonUser) throws Exception {
        try (final Session session = sessionFactory.openSession()) {
            try{
        session.getTransaction().begin();
        final Serializable save = session.save(commonUser);
        session.getTransaction().commit();
            return save != null;
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            }finally {
                session.close();
            }
        }return false;
    }

    @Override
    public CommonUser getById(Long id) throws Exception {
        try (final Session session = sessionFactory.openSession()) {
            try {
                session.getTransaction().begin();
                CommonUser commonUser = session.get(CommonUser.class, id);
                session.getTransaction().commit();
                return commonUser;

            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            } finally {
                session.close();

            }
        }return null;
    }

    @Override
    public boolean deleteUserById(Long id) throws Exception {
        try (final Session session = sessionFactory.openSession()) {
            try {
                session.getTransaction().begin();
                CommonUser commonUser = session.get(CommonUser.class, id);
                session.delete(commonUser);
                session.getTransaction().commit();
                CommonUser i = session.get(CommonUser.class, id);

                if (i == null) {
                    return true;
                }
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            } finally {
                session.close();
            }
        }return false;
    }

    @Override
    public void deleteUserByName(String firstName, String lastName) throws Exception {
    }

    @Override
    public CommonUser getByLogin(String login) throws Exception {
        try (final Session session = sessionFactory.openSession()) {
            final Query<CommonUser> query = session.createQuery("SELECT u FROM CommonUser u WHERE u.login = :login", CommonUser.class);
            query.setParameter("login", login);
            return query.getSingleResult();
        }
    }

    @Override
    public void updateUser() {

    }
    @Override
    public void getInfoAboutUser() {

    }

    private CommonUser fillUser(ResultSet resultSet) throws SQLException{
        final CommonUser user = new CommonUser();
        user.setId(resultSet.getLong("id"));
        user.setLogin(resultSet.getString("login"));
        user.setPassword(resultSet.getString("password"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.getUserRole().setId(resultSet.getInt("role_id"));
        return user;
    }
}
