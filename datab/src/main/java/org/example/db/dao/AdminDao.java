package org.example.db.dao;

import org.example.model.CommonUser;

public interface AdminDao {
    boolean createUser(CommonUser commonUser) throws Exception;
    CommonUser getById(Long id) throws Exception;
    boolean deleteUserById(Long id) throws Exception;

    CommonUser getByLogin(String login) throws Exception;
    void updateUser();
    void getInfoAboutUser();
    void deleteUserByName(String firstName, String lastName) throws Exception;

}
