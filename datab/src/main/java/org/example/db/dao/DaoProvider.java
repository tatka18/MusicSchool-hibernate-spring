package org.example.db.dao;


import org.example.db.dao.hibernate.impl.AdminDaoImpl;
import org.example.db.dao.hibernate.impl.CommonUserDaoImpl;
import org.example.db.dao.hibernate.impl.StudentDaoImpl;
import org.example.db.dao.hibernate.impl.TeacherDaoImpl;

public class DaoProvider {
    private static final DaoProvider instance = new DaoProvider();

    private DaoProvider(){};

    private CommonUserDao commonUserDao = new CommonUserDaoImpl();
    private AdminDao adminDao = new AdminDaoImpl();
    private StudentDao studentDao = new StudentDaoImpl();
    private TeacherDao teacherDao = new TeacherDaoImpl();

    public static DaoProvider getInstance(){
        return instance;
    }

    public CommonUserDao getCommonUserDao(){

        if (commonUserDao == null) {
            synchronized (CommonUserDao.class) {
                if (commonUserDao == null) {
                    commonUserDao = new CommonUserDaoImpl();
                }
            }
        }
        return commonUserDao;
    }

    public AdminDao getAdminDao(){
        if (adminDao == null) {
            synchronized (AdminDao.class) {
                if (adminDao == null) {
                    adminDao = new AdminDaoImpl();
                }
            }
        }
        return  adminDao;
    }

    public StudentDao getStudentDao(){

        if (studentDao == null) {
            synchronized (StudentDao.class) {
                if (studentDao == null) {
                    studentDao = new StudentDaoImpl();
                }
            }
        }
        return studentDao;
    }

    public TeacherDao getTeacherDao(){

        if (teacherDao == null) {
            synchronized (TeacherDao.class) {
                if (teacherDao == null) {
                    teacherDao = new TeacherDaoImpl();
                }
            }
        }
        return teacherDao;
    }

}
