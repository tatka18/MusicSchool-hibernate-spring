package org.example.db.dao.hibernate.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.HibernateUtil;
import org.example.db.connection.JdbcProvider;
import org.example.db.dao.TeacherDao;
import org.example.model.CommonUser;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TeacherDaoImpl implements TeacherDao {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);
    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public List<CommonUser> getStudentsFromGroup(int groupNumber){
        try(final Session session  = sessionFactory.openSession()) {
            try {
                session.getTransaction().begin();
                Query<?> list = session
                        .createQuery("select u.firstName, u.lastName from CommonUser u where u.id=u.student.id " +
                                "and u.student.group.id=(select g.id from Group g where g.groupNumber=?1)")
                        .setParameter(1, groupNumber);

                List<Object[]> resultList = (List<Object[]>) list.getResultList();
                List<CommonUser> commonUser = new ArrayList<>();
                for (Object[] results : resultList) {
                    CommonUser user = new CommonUser();
                    user.setFirstName((String) results[0]);
                    user.setLastName((String) results[1]);
                    commonUser.add(user);
                }
                return commonUser;
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            }finally {
                session.close();
            }
        }
        return null;
    }

    @Override
    public boolean setMarks(CommonUser commonUser) throws Exception {
//        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
//        try (final Session currentSession = sessionFactory.openSession()){
//            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();
//            final CriteriaQuery<CommonUser> query = criteriaBuilder.createQuery(CommonUser.class);
//
//            final Root<CommonUser> root = query.from(CommonUser.class);
//           root.
//            query.select(root);
//
//            final List<CommonUser> resultList = currentSession.createQuery(query).getResultList();
//
//        }

        try (final Session session = sessionFactory.openSession()) {
            try {
                session.getTransaction().begin();
                Query<?> setMark = session
                        .createSQLQuery("INSERT INTO marks (student_id, subject_id, day_id, mark) " +
                                "VALUES(SELECT u.id FROM users u WHERE first_name=?3 AND last_name=?4)," +
                                "(SELECT s.id FROM subjects s WHERE subject_name=?5), " +
                                "(SELECT d.id FROM dates d WHERE dates=?2), ?1)")
//                        .setParameter(1, commonUser.getMarks().getMark())
                        .setParameter(2, commonUser.getDates().getDate())
                        .setParameter(3, commonUser.getFirstName())
                        .setParameter(4, commonUser.getLastName())
                        .setParameter(5, commonUser.getSubject().getSubjectName());

                final int i = setMark.executeUpdate();
                if (i == 0) {
                    return false;
                }
                session.getTransaction().commit();
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            }finally {
                session.close();
            }
            return true;
        }
    }

    @Override
    public boolean set_attendance(CommonUser commonUser) throws Exception{
        try (Connection connection = JdbcProvider.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE attendance_report(subject_id, student_id, date_1)"
                    + "VALUES (?, ?, ?)")) {
                preparedStatement.setLong(1, commonUser.getSubject().getId());
                preparedStatement.setLong(2,commonUser.getStudent().getId());
                preparedStatement.setDate(3, commonUser.getSubject().getDates().getDate());

                final int i = preparedStatement.executeUpdate();
                if(i==0){
                    return false;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error during create User", e);
            throw new Exception("Error during create User", e);
        }
        return true;
    }

    @Override
    public List<Integer> getTeacherGroups(Long teacherId) throws Exception {

        try(final Session session  = sessionFactory.openSession()) {
            try {
                session.getTransaction().begin();
                Query<Integer> list = session.createQuery("select g.groupNumber from Group g join g.teachers tg where tg.id= ?1 ", Integer.class)
                        .setParameter(1, teacherId);
                List<Integer> groups = list.getResultList();

                return groups;
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            }finally {
                session.close();
            }
        }
        return null;
    }

    @Override
    public boolean ifMarkExist(CommonUser commonUser){
        try(final Session session = sessionFactory.openSession()) {
            try {
                session.getTransaction().begin();
                final Object result = session.find(CommonUser.class, commonUser);
                session.getTransaction().commit();
                return result != null;
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            } finally {
                session.close();
            }
        }return false;
    }

    @Override
    public void setNotesForStudent() {

    }

    private void fillCommonUserQuery(CommonUser commonUser, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, commonUser.getFirstName());
        preparedStatement.setString(2, commonUser.getLastName());
        preparedStatement.setString(3, commonUser.getSubject().getSubjectName());
        preparedStatement.setDate(4, commonUser.getDates().getDate());
        preparedStatement.setInt(5, commonUser.getStudent().getMark().getMark());
        System.out.println();
    }

    private void fillCommonUserQueryCheck(CommonUser commonUser, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, commonUser.getFirstName());
        preparedStatement.setString(2, commonUser.getLastName());
        preparedStatement.setString(3, commonUser.getSubject().getSubjectName());
        preparedStatement.setDate(4, commonUser.getSubject().getDates().getDate());
        System.out.println();
    }

    private CommonUser fillStudentsGroup(ResultSet resultSet) throws SQLException {
        CommonUser commonUser = new CommonUser();
        commonUser.setLastName(resultSet.getString("last_name"));
        commonUser.setFirstName(resultSet.getString("first_name"));

        return commonUser;
    }
}
