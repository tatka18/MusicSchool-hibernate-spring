package org.example.db.dao.spring.impl;

import org.example.db.dao.StudentDao;
import org.example.db.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentDaoImpl implements StudentDao {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<Integer> getMarks(Long id, String subjectName) throws Exception {


        return studentRepository.getMarks(id, subjectName);
    }
}
