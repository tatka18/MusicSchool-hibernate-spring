package org.example.db.dao;

import org.example.model.CommonUser;
import org.example.model.Student;

import java.util.List;

public interface StudentDao {
    List<Integer> getMarks(Long id, String subjectName) throws Exception;

}
