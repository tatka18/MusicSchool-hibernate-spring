package org.example.db.dao;

import org.example.model.CommonUser;

import java.util.Date;
import java.util.List;

public interface TeacherDao {
    List<CommonUser> getStudentsFromGroup(int groupNumber) throws Exception;
    boolean setMarks(CommonUser commonUser) throws Exception;
    boolean set_attendance(CommonUser commonUser) throws Exception;
    List<Integer> getTeacherGroups(Long teacherId) throws Exception;
    boolean ifMarkExist(CommonUser commonUser) throws Exception;

    void setNotesForStudent();
}
