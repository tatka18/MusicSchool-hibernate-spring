package org.example.db.dao.hibernate.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.HibernateUtil;
import org.example.db.connection.JdbcProvider;
import org.example.db.dao.StudentDao;
import org.example.model.CommonUser;
import org.example.model.Marks;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);
    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public List<Integer> getMarks(Long id, String subjectName) throws Exception {

        try(final Session session  = sessionFactory.openSession()) {
            try {
                session.getTransaction().begin();
                Query<Integer> list = session
                        .createQuery("select m.mark from Marks m where m.student.id=?1 " +
                                "and m.subject.id=(select s.id from Subject s where s.subjectName=?2)", Integer.class)
                        .setParameter(1, id)
                        .setParameter(2, subjectName);

                List<Integer> resultList  = list.getResultList();

                return resultList;
            } catch (HibernateException e) {
                session.getTransaction().rollback();
                e.printStackTrace();
            }finally {
                session.close();
            }
        }
        return null;
    }

    private int fillMarks(ResultSet resultSet) throws SQLException {
        int mark = resultSet.getInt("mark");
        return mark;

    }

}

