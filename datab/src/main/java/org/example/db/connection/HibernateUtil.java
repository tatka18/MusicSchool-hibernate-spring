package org.example.db.connection;

import org.example.model.CommonUser;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.InputStream;
import java.util.Properties;

public class HibernateUtil {
    private static EntityManagerFactory emf;

    private static SessionFactory sessionFactory;

    public static EntityManager getEntityManager(){
        if(emf == null){
            emf = Persistence.createEntityManagerFactory("test");
        }

        return emf.createEntityManager();
    }

    public static void close() {
        emf.close();
    } //закрываем entity manager factory  обязательно!!!

    public static SessionFactory getSessionFactory() {  //самая простая default-реализация SessionFactory
        if (sessionFactory == null) {
            synchronized (HibernateUtil.class) {
                if (sessionFactory == null) {
                    sessionFactory = new Configuration()
                            .configure()
                            .buildSessionFactory();

                    try {
                        final Properties properties = new Properties();
                        final InputStream in = JdbcProvider.class.getClassLoader().getResourceAsStream("connection.properties");
                        properties.load(in);

                        new Configuration()     //кастомизация(конфигурация с использованием java)
                                .configure("hibernate.cfg.xml")
                                .addAnnotatedClass(CommonUser.class)
                                .addProperties(properties)
                                .buildSessionFactory();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }return sessionFactory;
    }
    public static void closeSF(){ //закрываем session factory  обязательно!!!
        if (sessionFactory != null){
            sessionFactory.close();
        }
    }

}
