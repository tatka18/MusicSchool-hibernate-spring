<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>List</title>
</head>

<body style="background-color: rgba(82,53,53,0.15)">

<jsp:include page="WEB-INF/components/header.jsp">
    <jsp:param name="disableMenu" value="true"/>
</jsp:include>

<div align="center">
<%--<p1>Hello, <b><%=request.getSession().getAttribute("userName")%></b>!</p1><br/>--%>

    <c:if test="${sessionScope.roleId == 3}">
     <div align="center">
         <button>
             <a href="student_marks.jsp">to get my marks</a>
         </button>
    </div>
</c:if>

    <c:if test="${sessionScope.roleId == 2}">
<table>
        <form>
            <button formaction="group_list" formmethod="get" name="get_group" type="submit">
                to get my groups</button>
        </form>

        <form>
            <button formaction="set_mark" formmethod="post" name="set marks" type="submit">
                to set marks</button>
        </form>
        </table>
    </c:if>

    <c:if test="${sessionScope.roleId == 1}">
        <table>
            <td>
                <form>
                    <button formaction="admin" formmethod="get" name="action" type="submit" value="create_user">
                        create a new user</button>
                </form>
            </td>
            <td>
                <form>
                    <button formaction="admin" formmethod="get" name="action" type="submit" value="delete">
                        delete user by id</button>
                </form>
            </td>
            <td>
                <form>
                    <button formaction="admin" formmethod="get" name="action" type="submit" value="get_by_id">
                        get user by id</button>
                </form>
            </td>
        </table>
    </c:if>

    <jsp:include page="WEB-INF/components/footer.jsp">
        <jsp:param name="disableMenu" value="true"/>
    </jsp:include>
</div>


</body>
</html>
