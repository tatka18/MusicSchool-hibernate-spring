<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Marks</title>
</head>
<body body style="background-color: rgba(82,53,53,0.15)">

<jsp:include page="student_marks.jsp"/>
<%--запрашиваем данные о предмете для вывода и юзере--%>

<div align="center">
Your <b><%= request.getAttribute("subject_name")%> </b> marks, <%= request.getAttribute("username") %>!<br/><hr>

<c:forEach items="${requestScope.marks_list}" var="mark">  <%--передаем значения--%>
    <tr>
        <b><td><c:out value="${mark}" default="empty"/>  </td></b>
    </tr>
</c:forEach>

<br/>

    <%--Используем кнопку "Назад"--%>
    <input type="button" onclick="history.back();" value="Back" style="color: #402f30"/>
</div>
</body>
</html>
