<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body style="background-color: rgba(82,53,53,0.15)">
<jsp:include page="WEB-INF/components/header.jsp">
    <jsp:param name="disableMenu" value="true"/>
</jsp:include>
<div align="center">
    <h1><%=request.getSession().getAttribute("userName")%>, are You sure that you wanna Log Out?</h1>
</div>

<form action="logout" method="get">
    <div align="center">
        <input name="yes" type="submit" value="YES">
        <input name="no" type="submit" value="NO">
    </div>
</form>

<jsp:include page="WEB-INF/components/footer.jsp">
    <jsp:param name="disableMenu" value="true"/>
</jsp:include>
</body>
</html>
