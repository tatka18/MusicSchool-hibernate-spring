<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error 401</title>
</head>

<body>
<h1>You are not authorized. Please login or register</h1>
</body>
<jsp:include page="/login.jsp"></jsp:include>
</html>
