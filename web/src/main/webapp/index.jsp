<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Welcome Page - MS</title>
</head>
<jsp:include page="WEB-INF/components/header.jsp"/>

<hr>
<body style="background-color: rgba(82,53,53,0.15)">

<form>
    <button formaction="teacher_list" formmethod="get" name="list" type="submit" value="teachers">to get teachers</button>
</form>

<c:if test="${sessionScope.roleId != null}">
    <a href="users.jsp"><em>to the start page</em></a><br/>
    <input type="button" onclick="history.back();" value="Back" style="color: #402f30"/>
</c:if>

<c:if test="${sessionScope.userName == null}">
    <h2>Welcome to the web-site of our school</h2>
    <p><h3>Welcome to our Music School!!</h3></p><br/>
    <p><em>We are glad to see you here. Our teachers can help you to start your life in a new way.
        Just join us. We will help you with big big pleasure</em>
    <hr>

    <fieldset>
        LogIn or Register if you wanna Join
        </p>
        <p>
            Our School is the best school in our area! If you wanna to get a good aducation WELCOME to our family!
        </p>
        <p>Look down and join us

            <br/>
        </p>
    </fieldset>
    <h4>Thank you for your attention. Waiting for you on our lessons. If you wanna join us please

        <a href="login.jsp"><em>login here</em></a> or
        <a href="register.jsp"><em>register</em></a>

    </h4>

</c:if>

<jsp:include page="WEB-INF/components/footer.jsp"/>
</body>
</html>
