<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Attendance Report</title>
    <style>
        table {width: 50%;
            border-collapse: collapse;
            border: 2px solid #ccc;
            margin-bottom: 2em;
        }
        table td{
            border: 2px solid #ccc;
            padding: 7px;
        }
        table th{
            border: 2px solid #ccc;
            padding: 7px;
        }
    </style>
</head>
<body style="background-color: rgba(82,53,53,0.15)">
<jsp:include page="WEB-INF/components/header.jsp"/>
<br/>

<table>
    <tr>
        <th>№</th>
        <th>Surname</th>
        <th>Name</th>
    </tr>

    <c:forEach items="${requestScope.student_list}" var="user" varStatus="pos" >  <%--передаем значения--%>
        <tr><%--заполняем ячейки --%>
            <td>${pos.index + 1}</td>
            <td><c:out value="${user.lastName}" default="empty" /></td>
            <td><c:out value="${user.firstName}" default="empty" /></td>
    <div>
        <form action="attendance_list">
        <td><input type="checkbox">+</td>
        </form>
    </div>

        </tr>
    </c:forEach>


</table>
<jsp:include page="WEB-INF/components/footer.jsp"/>

</body>
</html>
