package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.services.ServiceProvider;
import org.example.services.TeacherService;
import org.example.web.util.WebConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AttendanceServlet", urlPatterns = "/attendance_list")
public class AttendanceServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    final TeacherService teacherService = serviceProvider.getTeacherService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        final String attendance = req.getParameter(WebConstants.ATTENDANCE_LIST);

//        try {
//            List<CommonUser> list = teacherService.getStudents(role);
//            LOGGER.info(list.toString());
//            req.setAttribute("student_list", list);
//            req.getServletContext().getRequestDispatcher("/students.jsp").forward(req, resp);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        super.doGet(req, resp);
    }
}
