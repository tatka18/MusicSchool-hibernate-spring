package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.model.CommonUser;
import org.example.services.AdminService;
import org.example.services.ServiceProvider;
import org.example.web.util.WebConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GetByIdServlet", urlPatterns = "/get_by_id")
public class AdminGetByIdServlet  extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    private static final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    private static final AdminService adminService = serviceProvider.getAdminService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        final String userId = req.getParameter(WebConstants.ID_PARAM);
        String message;

        try {
            CommonUser user = adminService.getUserById(userId);
            if(user == null){
                message = "User with id " + userId + " doesn't exist!";
            }else {
                message = "User with id " + userId + ": " +user.getFirstName() + " " + user.getLastName() + " - " + user.getUserRole().getName();
            }
            final RequestDispatcher dispatcher = req.getRequestDispatcher("/admin.jsp");
            req.setAttribute("result", message);
            resp.setContentType("text/html");
            dispatcher.include(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
