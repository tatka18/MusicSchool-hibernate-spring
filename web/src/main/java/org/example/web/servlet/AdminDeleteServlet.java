package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.services.AdminService;
import org.example.services.CommonUserService;
import org.example.services.ServiceProvider;
import org.example.web.util.WebConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "DeleteServlet", urlPatterns = "/delete")
public class AdminDeleteServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    private static final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    private static final CommonUserService commonService = serviceProvider.getCommonUserService();
    private static final AdminService adminService = serviceProvider.getAdminService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        final String userId = req.getParameter(WebConstants.ID_PARAM);

        try {
            String message;
            boolean result = adminService.deleteUserById(userId);
            if(!result){
                message = "User with ID " + userId +" is not found!";
            }else {
                message = "User with ID " + userId + " has been successfully deleted!";
            }
            final RequestDispatcher dispatcher = req.getRequestDispatcher("/admin.jsp");
            req.setAttribute("result", message);
            resp.setContentType("text/html");
            dispatcher.include(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
