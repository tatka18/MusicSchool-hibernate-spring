package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;

import org.example.services.ServiceProvider;
import org.example.services.TeacherService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static org.example.web.filter.AuthFilter.USER_ID_PAR;

@WebServlet(name = "GroupListServlet", urlPatterns = "/group_list")
public class GroupListServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    final TeacherService teacherService = serviceProvider.getTeacherService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            final HttpSession session = req.getSession();
            String id = session.getAttribute(USER_ID_PAR).toString();

            LOGGER.info("in servlet " + id);
            resp.setContentType("text/html");

            List<Integer> list = teacherService.getTeacherGroups(id);

            LOGGER.info("--------" + list.toString());
            req.setAttribute("group_list", list);
            final RequestDispatcher dispatcher = req.getRequestDispatcher("/groups_list.jsp");
            dispatcher.forward(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
        }
        super.doGet(req, resp);
    }
}
