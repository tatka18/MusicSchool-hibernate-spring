package org.example.web.servlet;

import com.google.protobuf.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.services.CommonUserService;
import org.example.services.ServiceProvider;
import org.example.web.util.WebConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RegistrationServlet", urlPatterns = "/registration")
public class RegistrationServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    final CommonUserService commonService = serviceProvider.getCommonUserService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final String login = req.getParameter(WebConstants.LOGIN_PARAM);
        final String password = req.getParameter(WebConstants.PASSWORD_PARAM);
        final String name = req.getParameter(WebConstants.FIRST_NAME_PARAM);
        final String surname = req.getParameter(WebConstants.LAST_NAME_PARAM);
        final String role = req.getParameter(WebConstants.ROLE_PARAM);

        boolean result;
        try{
            final String contextPath = req.getContextPath();
                result = commonService.registration(login, password, name, surname, role);
                if(result){
                    resp.getWriter().write("<p>CONGRATS! YOU ARE REGISTERED! NOW LOGIN PLEASE</p>");
                    final RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");

                dispatcher.include(req, resp);
                    //resp.sendRedirect(contextPath + "/login.jsp");
                }else {
                    resp.sendRedirect(contextPath + "/404.jsp");
                }

        }catch (ServiceException | ServletException e){
            LOGGER.error("Error During Registration Servlet");
        }
    }
}

