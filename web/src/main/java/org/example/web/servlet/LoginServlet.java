package org.example.web.servlet;

import com.google.protobuf.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.db.dao.DaoException;
import org.example.model.CommonUser;
import org.example.services.CommonUserService;
import org.example.services.ServiceProvider;
import org.example.web.util.WebConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import java.io.IOException;
import java.sql.SQLException;

import static org.example.web.filter.AuthFilter.USER_ID_PAR;

@WebServlet(name = "LoginServlet", urlPatterns = "/auth")

public class LoginServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);
    private static final String USER_NAME_PARAM= "userName";
    private static final String USER_SURNAME_PARAM= "userSurname";
    private static final String USER_ROLE_ID_PARAM = "roleId";

    final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    final CommonUserService commonUserService = serviceProvider.getCommonUserService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String login = req.getParameter(WebConstants.LOGIN_PARAM);
        final String password = req.getParameter(WebConstants.PASSWORD_PARAM);

        try {
            final CommonUser user = commonUserService.authorize(login, password);

            final String contextPath = req.getContextPath();

            if (user == null) {
                resp.getWriter().write("<p>We didnt find the user with this login. Probably You are not registered</p>");
                resp.sendRedirect(contextPath + "/login.jsp");
            } else {
                final HttpSession session = req.getSession();   //устанавливаем сессию
                session.setAttribute(USER_ID_PAR, user.getId());
                session.setAttribute(USER_NAME_PARAM, user.getFirstName());
                session.setAttribute(USER_SURNAME_PARAM, user.getLastName());
                session.setAttribute(USER_ROLE_ID_PARAM, user.getUserRole().getId());

                final Cookie projectCookie = new Cookie("projectCookie", "PC");
                resp.addCookie(projectCookie);
                projectCookie.setMaxAge(-1);        //кука будет удалена после закрытия браузера

                req.setAttribute("username", user.getFirstName() + " " + user.getLastName());
                resp.setContentType("text/html");
                resp.setIntHeader("lesson-header", 6);

                final RequestDispatcher dispatcher = req.getRequestDispatcher("/users.jsp");
//                 resp.getWriter().write("<p>Welcome to the site of our school!</p>");
                dispatcher.include(req, resp);
            }
        }catch (ServiceException e){
            LOGGER.error("null login null password");
        } catch (DaoException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
