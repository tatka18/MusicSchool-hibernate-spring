package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.model.Subject;
import org.example.services.CommonUserService;
import org.example.services.ServiceProvider;
import org.example.web.util.WebConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SubjectServlet", urlPatterns = "/subject")
public class SubjectServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    final CommonUserService commonUserService = serviceProvider.getCommonUserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String subjectName = req.getParameter(WebConstants.SUBJECT_NAME);

        LOGGER.info("subject_name " + subjectName);
        try {
            Subject subjectInfo = commonUserService.getSubjectInfo(subjectName);
            req.setAttribute("subject_info", subjectInfo.getSpecification());
            final RequestDispatcher dispatcher = req.getRequestDispatcher("/subject_info.jsp");
            dispatcher.forward(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Error During Registration Servlet");
        }


    }
}
