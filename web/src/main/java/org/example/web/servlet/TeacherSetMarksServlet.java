package org.example.web.servlet;

import com.google.protobuf.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.services.ServiceProvider;
import org.example.services.TeacherService;
import org.example.web.util.WebConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SetMarksServlet", urlPatterns = "/set_marks")
public class TeacherSetMarksServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    final TeacherService teacherService = serviceProvider.getTeacherService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        final String name = req.getParameter(WebConstants.FIRST_NAME_PARAM);
        final String surname = req.getParameter(WebConstants.LAST_NAME_PARAM);
        final String mark = req.getParameter(WebConstants.MARK);
        final String date = req.getParameter(WebConstants.DATE);
        final String subjectName = req.getParameter(WebConstants.SUBJECT_NAME);

        boolean result;

        try{
            final String contextPath = req.getContextPath();
            result = teacherService.setMarks(name, surname, mark, date ,subjectName);
            if(result){
                final RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
                //   resp.getWriter().write("<p>CONGRATS! YOU ARE REGISTERED! NOW LOGIN PLEASE</p>");
                dispatcher.forward(req, resp);
                //resp.sendRedirect(contextPath + "/login.jsp");
            }else {
                resp.sendRedirect(contextPath + "/404.jsp");
            }

        }catch (ServiceException | ServletException e){
            LOGGER.error("Error During Registration Servlet");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
