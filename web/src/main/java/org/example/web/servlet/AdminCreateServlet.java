package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.model.CommonUser;
import org.example.services.AdminService;
import org.example.services.ServiceProvider;
import org.example.web.util.WebConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AdminCreateServlet", urlPatterns = "/create_user")
public class AdminCreateServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    final AdminService adminService = serviceProvider.getAdminService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final String login = req.getParameter(WebConstants.LOGIN_PARAM);
        final String password = req.getParameter(WebConstants.PASSWORD_PARAM);
        final String name = req.getParameter(WebConstants.FIRST_NAME_PARAM);
        final String surname = req.getParameter(WebConstants.LAST_NAME_PARAM);
        final String role = req.getParameter(WebConstants.ROLE_PARAM);

        try {
            CommonUser user = adminService.createUser(login, password, name, surname, role);
            final RequestDispatcher dispatcher = req.getRequestDispatcher("/admin.jsp");
            req.setAttribute("result", "User has been successfully added!");
            resp.setContentType("text/html");
            dispatcher.include(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
