package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.web.util.WebConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(name = "LogOutServlet", urlPatterns = "/logout")
public class LogOutServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String yes = req.getParameter(WebConstants.LOGOUT_YES);

        final String contextPath = req.getContextPath();
        resp.setContentType("text/html");

        if(yes != null){
            final HttpSession session = req.getSession(false);
            if (session != null) {
                session.invalidate();
            }
            final RequestDispatcher dispatcher = req.getRequestDispatcher("/index.jsp");
            dispatcher.forward(req, resp);

        }else{
                final RequestDispatcher dispatcher = req.getRequestDispatcher("/users.jsp");
                dispatcher.forward(req, resp);
        }
        LOGGER.info("Session has been invalidated");
        resp.sendRedirect(contextPath + "/index.jsp");
    }
}