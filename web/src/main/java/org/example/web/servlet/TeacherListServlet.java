package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.model.Teacher;
import org.example.services.CommonUserService;
import org.example.services.ServiceProvider;
import org.example.services.TeacherService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "TeacherListServlet", urlPatterns = "/teacher_list")
public class TeacherListServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    final TeacherService teacherService = serviceProvider.getTeacherService();
    final CommonUserService commonUserService = serviceProvider.getCommonUserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            List<Teacher> list = commonUserService.getTeachers();

            System.out.println();
            resp.setContentType("text/html");

            LOGGER.info(list.toString());
            req.setAttribute("teacher_list", list);
            req.getServletContext().getRequestDispatcher("/teachers.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        super.doGet(req, resp);
    }


}
