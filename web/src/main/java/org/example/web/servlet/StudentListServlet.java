package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.model.CommonUser;
import org.example.services.CommonUserService;
import org.example.services.ServiceProvider;
import org.example.services.TeacherService;
import org.example.web.util.WebConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "StudentListServlet", urlPatterns = "/student_list")
public class StudentListServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    final CommonUserService commonUserService = serviceProvider.getCommonUserService();
    final TeacherService teacherService = serviceProvider.getTeacherService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        final String group = req.getParameter(WebConstants.GROUP_NUMBER);

        try {
            resp.setContentType("text/html");

                List<CommonUser> list = teacherService.getListStudentsByGroup(group);
            System.out.println();
                req.setAttribute("student_list", list);
                req.getServletContext().getRequestDispatcher("/students.jsp").forward(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
        }
        super.doGet(req, resp);
    }
}
