package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.services.CommonUserService;
import org.example.services.ServiceProvider;
import org.example.web.util.WebConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AdminServlet", urlPatterns = "/admin")
public class AdminServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    private static final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    private static final CommonUserService commonService = serviceProvider.getCommonUserService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String action = req.getParameter(WebConstants.ADMIN_ACTION);

       LOGGER.info("Action " + action);

        try {
            req.setAttribute("action", action);

            final RequestDispatcher dispatcher = req.getRequestDispatcher("/admin.jsp");
            dispatcher.include(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Error During Registration Servlet");
        }
    }
}

