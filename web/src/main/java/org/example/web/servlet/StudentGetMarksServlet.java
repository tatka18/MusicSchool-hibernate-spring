package org.example.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.model.CommonUser;
import org.example.services.CommonUserService;
import org.example.services.ServiceProvider;
import org.example.services.StudentService;
import org.example.web.util.WebConstants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.util.List;

import static org.example.web.filter.AuthFilter.USER_ID_PAR;

@WebServlet(name = "StudentGetMarksServlet", urlPatterns = "/student_get_marks")
public class StudentGetMarksServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProvider.class);

    final ServiceProvider serviceProvider = ServiceProvider.getInstance();
    final StudentService studentService = serviceProvider.getStudentService();
    final CommonUserService commonUserService = serviceProvider.getCommonUserService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        final String subject = req.getParameter(WebConstants.SUBJECT_CHOOSE);
//        LOGGER.info(subject);

        try {
            final HttpSession session = req.getSession();   //устанавливаем сессию
            String id = session.getAttribute(USER_ID_PAR).toString();

            CommonUser user = commonUserService.getUser(id);

            List<Integer> marksList = studentService.getMarks(id, subject);
            resp.setContentType("text/html");

            final RequestDispatcher rd = req.getRequestDispatcher("/marks.jsp");
            req.setAttribute("username", user.getFirstName() + " " + user.getLastName());  //устанавливаем атрибут для jsp
            req.setAttribute("subject_name", subject);      //устанавливаем атрибут для jsp
            req.setAttribute("marks_list", marksList);       //устанавливаем атрибут для jsp
            rd.include(req, resp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
