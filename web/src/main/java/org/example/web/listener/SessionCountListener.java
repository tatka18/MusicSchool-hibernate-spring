package org.example.web.listener;

import org.example.services.SessionCountService;
import org.example.services.impl.SessionCountServiceImpl;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionCountListener implements HttpSessionListener {
    private final SessionCountService sessionCountService = new SessionCountServiceImpl();

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
    final int count = sessionCountService.add();
        System.out.println("Session added count: " + count);

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
     final int count = sessionCountService.add();
        System.out.println("Session added count: " + count);
    }
}
