package org.example.web.filter;

import org.example.web.util.WebConstants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter(filterName = "LoginFilter",
        urlPatterns = "/auth"
)
public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse resp = (HttpServletResponse) response;

        if(req.getParameter(WebConstants.LOGIN_PARAM).isEmpty() || req.getParameter(WebConstants.PASSWORD_PARAM).isEmpty()){

            final RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
            resp.getWriter().write("<p>LOGIN AND PASSWORD CAN'T BE EMPTY! PLEASE, TRY AGAIN!</p>");
            dispatcher.include(req, resp);
        }
        else {chain.doFilter(request, response);}
    }

    @Override
    public void destroy() {

    }
}
