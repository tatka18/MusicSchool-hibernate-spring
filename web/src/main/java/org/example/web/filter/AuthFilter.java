package org.example.web.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.stream.Stream;

@WebFilter(filterName = "AuthFilter",
        urlPatterns = "/*",
        initParams = {
                @WebInitParam(name = AuthFilter.IS_ACTIVE_FILTER_PAR,
                        value = "true",
                        description = "activation Of Filter")
        }
)
public class AuthFilter implements Filter {
    public static final String USER_ID_PAR = "userId";
    public static final  String IS_ACTIVE_FILTER_PAR  = "isActive";
    private boolean active;

    @Override
    public void init(FilterConfig filterConfig){
        final String isActiveString = filterConfig.getInitParameter(IS_ACTIVE_FILTER_PAR);
        active = isActiveString == null || isActiveString.toLowerCase().equals("true");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse resp = (HttpServletResponse) response;;

    if(!active || sessionValid(req) || byPassFilter(req)){
            chain.doFilter(request, resp);
        }else{
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @Override
    public void destroy() {
    //stub
    }
    private boolean sessionValid(HttpServletRequest request){
        final HttpSession session = request.getSession();
        return session != null && session.getAttribute(USER_ID_PAR) != null;
    }

    private boolean byPassFilter(HttpServletRequest request) {
        final String contextPath = request.getContextPath();
        final String path = request.getRequestURI().replaceFirst(contextPath, "");
        return Stream.of("/", "/index.jsp", "/login.jsp", "/register.jsp", "/registration", "/auth", "/teacher_list", "/subject").anyMatch(path::equalsIgnoreCase);
    }
}
