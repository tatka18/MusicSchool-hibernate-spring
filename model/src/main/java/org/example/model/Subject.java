package org.example.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "subjects")
public class Subject {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "subject_name", unique = true, nullable = false)
    private String subjectName;

    @Column(name = "subject_info")
    private String specification;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private CommonUser commonUser;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "commonUser", cascade = CascadeType.ALL)
    private Student student;

    @OneToMany(mappedBy = "subject", fetch = FetchType.LAZY)
    private List<Marks> marks = new ArrayList<Marks>();

    @OneToMany(mappedBy = "subject", fetch = FetchType.LAZY)
    private List<Teacher> teacherList = new ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private Dates dates;

    public Subject(){}

    public Subject(String name){
        this.subjectName= name;
    }

    public Subject(Long id, String name, String specification, Dates dates) {
        this.id = id;
        this.subjectName = name;
        this.specification = specification;
        this.dates = dates;
    }

    public Subject(String subjectName, String specification) {
        this.subjectName = subjectName;
        this.specification = specification;
    }

    public List<Teacher> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<Teacher> teacherList) {
        this.teacherList = teacherList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Dates getDates() {
        return dates;
    }

    public void setDates(Dates dates) {
        this.dates = dates;
    }

    public CommonUser getCommonUser() {
        return commonUser;
    }

    public void setCommonUser(CommonUser commonUser) {
        this.commonUser = commonUser;
    }

    public List<Marks> getMarks() {
        return marks;
    }

    public void setMarks(List<Marks> marks) {
        this.marks = marks;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
}
