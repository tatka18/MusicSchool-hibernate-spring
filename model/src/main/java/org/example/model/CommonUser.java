package org.example.model;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.JOINED)
public class CommonUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login", nullable = false, length = 50, unique = true)
    private String login;
    @Column(name = "password", nullable = false, length = 50)
    private String password;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "commonUser", cascade = CascadeType.ALL)
    private Student student;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "commonUser", cascade = CascadeType.ALL)
    private Teacher teacher;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "commonUser", cascade = CascadeType.ALL)
    private Group group;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "commonUser", cascade = CascadeType.ALL)
    private Subject subject;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "commonUser", cascade = CascadeType.ALL)
    private Dates dates;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "commonUser", cascade = CascadeType.ALL)
    private Marks marks;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "role_id")
    private UserRole userRole;

    public CommonUser() {
    }

    public CommonUser(Long id){
        this.id = id;
    }

    public CommonUser(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public CommonUser(String login, String password, String firstName, String lastName, UserRole userRole) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userRole = userRole;
    }
    public CommonUser(String firstName, String lastName, Subject subject, Student student) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.subject = subject;
        this.student = student;
    }

    public CommonUser(Long id, String login, String password, String firstName, String lastName) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public CommonUser(String login, String password, String firstName, String lastName) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public CommonUser(String firstName, String lastName, Subject subject, Dates dates, Marks marks) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.subject = subject;
        this.dates = dates;
        this.marks = marks;
    }

    public CommonUser(Long id, String firstName, String lastName, UserRole userRole) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userRole = userRole;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Marks getMarks() {
        return marks;
    }

    public void setMarks(Marks marks) {
        this.marks = marks;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public void setDates(Dates dates) {
        this.dates = dates;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public Dates getDates() {
        return dates;
    }

    public void setDate(Dates dates) {
        this.dates = dates;
    }

    @Override
    public String toString() {
        return "CommonUser{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", student=" + student +
                ", teacher=" + teacher +
                ", group=" + group +
                ", subject=" + subject +
                ", dates=" + dates +
                ", marks=" + marks +
                ", userRole=" + userRole +
                '}';
    }
}

