package org.example.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user_role")
public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "userRole", fetch = FetchType.LAZY)
    private List<CommonUser> commonUserList = new ArrayList<>();

    public UserRole() {
    }

    public UserRole(int id) {
        this.id = id;
    }

    public UserRole(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CommonUser> getCommonUserList() {
        return commonUserList;
    }

    public void setCommonUserList(List<CommonUser> commonUserList) {
        this.commonUserList = commonUserList;
    }

}
